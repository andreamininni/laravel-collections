<?php

use App\Models\Product;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('brand');
            $table->string('category');
            $table->float('price', 6, 2);
            $table->timestamps();
        });

        $products = [
            [
                'name' => 'msi modern 14',
                'brand' => 'msi',
                'category' => 'Electronics',
                'price' => 899.00
            ],
            [
                'name' => 'msi prestige 14',
                'brand' => 'msi',
                'category' => 'Electronics',
                'price' => 999.00
            ],
            [
                'name' => 'MacBook Pro 16',
                'brand' => 'Apple',
                'category' => 'Electronics',
                'price' => 3200.00
            ],
            [
                'name' => 'Acer ProBook 16',
                'brand' => 'Acer',
                'category' => 'Electronics',
                'price' => 899.00
            ],
            [
                'name' => 'Asus VivoBook Pro',
                'brand' => 'Asus',
                'category' => 'Electronics',
                'price' => 1000.00
            ],
            [
                'name' => 'Lenovo X1 Carbon',
                'brand' => 'Lenovo',
                'category' => 'Electronics',
                'price' => 1200.00
            ],
            [
                'name' => 'Levono IdeaPad',
                'brand' => 'Lenovo',
                'category' => 'Electronics',
                'price' => 400.00
            ],
            [
                'name' => 'Microsoft Surface Pro',
                'brand' => 'Microsoft',
                'category' => 'Electronics',
                'price' => 1500.00
            ],
            [
                'name' => 'msi modern 14',
                'brand' => 'msi',
                'category' => 'Electronics',
                'price' => 899.00
            ],
            [
                'name' => 'Huawei Matebook 14',
                'brand' => 'Huawei',
                'category' => 'Electronics',
                'price' => 650.00
            ],
            [
                'name' => 'Sony A73',
                'brand' => 'Sony',
                'category' => 'Photography',
                'price' => 1200.00
            ],
            [
                'name' => 'Sony A99',
                'brand' => 'Sony',
                'category' => 'Photography',
                'price' => 900.00
            ],
            [
                'name' => 'Canon 5D',
                'brand' => 'Canon',
                'category' => 'Photography',
                'price' => 2000.00
            ],
            [
                'name' => 'Canon 6D Mk II',
                'brand' => 'Canon',
                'category' => 'Photography',
                'price' => 1200.00
            ],
            [
                'name' => 'Nikon D750',
                'brand' => 'Nikon',
                'category' => 'Photography',
                'price' => 1800.00
            ],
            [
                'name' => 'PlayStation 5',
                'brand' => 'Sony',
                'category' => 'Gaming',
                'price' => 499
            ],
            [
                'name' => 'Xbox Series X',
                'brand' => 'Microsoft',
                'category' => 'Gaming',
                'price' => 499
            ],
            [
                'name' => 'PlayStation 4 Pro',
                'brand' => 'Sony',
                'category' => 'gaming',
                'price' => 350.00
            ],
            [
                'name' => 'Asus Rog Delta',
                'brand' => 'Asus',
                'category' => 'Audio',
                'price' => 195.00
            ],
            [
                'name' => 'Asus Rog Strix Go 2.4',
                'brand' => 'Asus',
                'category' => 'Audio',
                'price' => 200.00
            ],
            [
                'name' => 'CoolerMaster MH670',
                'brand' => 'CoolerMaster',
                'category' => 'Audio',
                'price' => 120.00
            ],
            [
                'name' => 'CoolerMaster MH630',
                'brand' => 'CoolerMaster',
                'category' => 'Audio',
                'price' => 50.00
            ],
            [
                'name' => 'CoolerMaster MH650',
                'brand' => 'CoolerMaster',
                'category' => 'Audio',
                'price' => 80.00
            ],
            [
                'name' => 'Philips SHC5200',
                'brand' => 'Philips',
                'category' => 'Audio',
                'price' => 120.00
            ],
            [
                'name' => 'Philips TAPH802',
                'brand' => 'Philips',
                'category' => 'Audio',
                'price' => 100.00
            ],
            [
                'name' => 'Sony MDR-FR81',
                'brand' => 'Sony',
                'category' => 'Audio',
                'price' => 80.00
            ],
            [
                'name' => 'Xiaomi Note 10',
                'brand' => 'Xiaomi',
                'category' => 'Phone',
                'price' => 500.00
            ],
            [
                'name' => 'Xiaomi Note 9 Pro',
                'brand' => 'Xiaomi',
                'category' => 'Phone',
                'price' => 250.00
            ],
            [
                'name' => 'Samsung Galaxy A21',
                'brand' => 'Samsung',
                'category' => 'Phone',
                'price' => 180.00
            ],
            [
                'name' => 'Samsung Galaxy A80',
                'brand' => 'Samsung',
                'category' => 'Phone',
                'price' => 650.00
            ],
            [
                'name' => 'Samsung Galaxy S20',
                'brand' => 'Samsung',
                'category' => 'Phone',
                'price' => 664.00
            ],
            [
                'name' => 'iPhone 12',
                'brand' => 'Apple',
                'category' => 'Phone',
                'price' => 1000.00
            ],
            [
                'name' => 'iPhone 12 Pro',
                'brand' => 'Apple',
                'category' => 'Phone',
                'price' => 1200.00
            ],
            [
                'name' => 'iPhone 12 Pro Max',
                'brand' => 'Apple',
                'category' => 'Phone',
                'price' => 1500.00
            ],
            [
                'name' => 'Huawei P40 Lite',
                'brand' => 'Huawei',
                'category' => 'Phone',
                'price' => 165.00
            ],
            [
                'name' => 'Huawei P40',
                'brand' => 'Huawei',
                'category' => 'Phone',
                'price' => 400.00
            ],
            [
                'name' => 'Huawei Mate 30 Pro',
                'brand' => 'Huawei',
                'category' => 'Phone',
                'price' => 925.00
            ],
            [
                'name' => 'Huawei P30 Lite',
                'brand' => 'Huawei',
                'category' => 'Phone',
                'price' => 670.00
            ]
        ];

        foreach($products as $p){
            $x = new Product();
            $x->name = $p['name'];
            $x->brand = $p['brand'];
            $x->category = $p['category'];
            $x->price = $p['price'];
            $x->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
